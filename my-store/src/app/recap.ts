// Declare a variable
const username: string = 'Nicolas';

// Create function
const sum = (a: number, b: number) => {
    return a + b;
}
sum(1, 2);

// Pattern Object Oriented
class Persona {

    constructor(public age: number, public lastName: string) {}

}

const nico = new Persona(15, 'Molina');
nico.lastName;