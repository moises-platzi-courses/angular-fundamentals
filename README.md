# Angular Fundamentals

Instala Angular CLI y aprende a usar sus principales comandos. Descubre cómo escuchar y responder a eventos generados por el usuario. Define estructuras de control y aplica estilos en la creación de una tienda en línea.

+ Descubre cómo se da la comunicación de datos con Event Binding.
+ Comprende la estructura de un proyecto en Angular.
+ Aprende sobre estructuras de control con NgModel.
+ Agrega estilos a la lista de productos y crea un formulario.

## Contenido del Curso

### Primeros pasos
+ Qué es Angular y cómo aprenderlo
+ Instalación de Angular CLI
+ Comandos de Angular para correr tu proyecto
+ Estructuras de un proyecto de Angular
+ Conceptos básicos de TypeScript para usar Angular

### Comunicación de datos
+ String Interpolation
+ Property Binding
+ Introducción al Event Binding de Angular
+ Otros eventos que puedes escuchar
+ Data binding con ngModel

### Estructuras de control
+ Uso de *ngIf
+ Uso de *ngFor
+ *ngFor para arrays
+ Uso de *ngSwitch
+ Angular Devtools

### Estilos
+ Estilos a la lista de productos
+ Class and style
+ NgClass y NgStyle
+ Crear un formulario

### Deploy
+ Despliegue con Firebase Hosting
